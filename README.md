[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
# GUIÓN DE PRÁCTICAS[^nota1]
## Sesión 12: Java Message Service (II)

Es este segundo guión de introducción a **JMS** se presentan los siguientes elementos para el desarrollo de las prácticas de la asignatura:

1. Enviar y recibir objetos Java como contenido de los mensajes **JMS**.
2. Modos en el envío y recepción de los mensajes **JMS**.
3. Recepción asíncrona de mensajes.
4. Posible implementación de la sentencia `select` utilizada en la resolución teórica de los ejercicios de la asignatura. 

## 12.1 Envío y recepción de objetos Java

Para el desarrollo de las prácticas de la asignatura necesitamos incluir los objetos Java creados para el intercambio de información entre los diferentes procesos. Los objetos deben formar parte del contenido del mensaje **JMS** y para ello utilizaremos los mensajes cuyo contenido es un objeto `String` ([`TextMessage`](https://docs.oracle.com/javaee/7/api/javax/jms/TextMessage.html "interface in javax.jms")). Para ello debemos transformar el objeto Java que queremos enviar a un objeto `String` que lo representa para enviarlo y la transformación inversa cuando recibimos el mensaje y obtenemos el contenido. Para ello utilizaremos la biblioteca [**Gson**](https://es.wikipedia.org/wiki/Gson) que nos permite una implementación para el formato [**JSON**](https://es.wikipedia.org/wiki/JSON). De esta forma *serializamos*, lo transformamos en una secuencia de *bytes*, el objeto Java a una representación de texto, objeto `String`, para incluirlo en el contenido del mensaje. También disponemos de la transformación inversa, es decir, de un objeto `String` en formato **JSON** obtenemos su equivalente objeto Java. En el guión solo presentaré una forma muy elemental para utilizar las posibilidades que nos proporciona la biblioteca **Gson** que permite desarrollar convenientemente las prácticas de la asignatura. Para una documentación sobre la biblioteca se puede consultar el libro [Instant GSON](https://learning.oreilly.com/library/view/instant-gson/9781783282036/)[^nota1].

### 12.1.1 Trabajar con la biblioteca Gson 

Para incluir las dependencias de la biblioteca **Gson** en nuestros proyectos **NetBeans** hay que modificar el fichero pom.xml incluyendo lo siguiente:

```xml
<dependencies>
...
    <dependency>
        <groupId>com.google.code.gson</groupId>
        <artifactId>gson</artifactId>
        <version>2.8.5</version>
    </dependency>
...
</dependencies>
```

### 12.1.2 Transformaciones JSON

La biblioteca Gson nos proporciona la clase [`GsonBuilder`](https://www.javadoc.io/static/com.google.code.gson/gson/2.8.5/com/google/gson/GsonBuilder.html "class in com.google.gson") para transformar un objeto Java a su equivalencia **JSON** y viceversa. Para las prácticas podemos utilizar la clase `GsonUtil<T>` que se incluye en este guión.

Esta clase incluye los siguientes métodos:

- `public String encode(T contenido, Class<T> typeParameterClass)` : Es el método que utilizaremos para transformar un objeto Java y obtener su equivalente **JSON** representado por un objeto `String`. Esta cadena de texto es lo que formará parte del contenido del mensaje.

```java
public String encode(T contenido, Class<T> typeParameterClass) {
    if( contenido != null )
        return gson.toJson(contenido, typeParameterClass);
    else
        throw new NullPointerException();
}
```

- `public T decode(String contenido, Class<T> typeParameterClass)` : Es el método que utilizaremos para transformar un objeto `String` con la representación JSON a su equivalente objeto Java. Así transformamos el contenido del mensaje en el objeto Java que utiliza el proceso para su funcionamiento.

```java
public T decode(String contenido, Class<T> typeParameterClass) {
    if( contenido != null )
        return gson.fromJson(contenido, typeParameterClass);
    else
        throw new NullPointerException();
}
```

### 12.1.3 Ejemplo de utilización

En este ejemplo el proceso `Producer` envía un número de mensajes establecido cuyo contenido contiene un `Resource` que el proceso `Consumer` recibe de forma síncrona para poder tratarlo adecuadamente. En este caso solo lo muestra por la consola del sistema.

1. Definimos la clase `Producer` de forma similar a como se ha definido en el guión de la [sesión 11](https://gitlab.com/ssccdd/guionsesion11#1157-ejemplo-de-prueba).
2. El método `execution()` es el que hay que adaptar para la creación de una instancia de `Resource` para incluirlo en el contenido de los mensajes que se envían.
3. Se establece la conexión para el envío de los mensajes y creamos un objeto `UtilGson<Resource>` para la transformación del objeto `Resource` en el contenido del mensaje de texto que se enviará.

```java
...
MessageProducer producer = session.createProducer(destination);
// Creamos la instancia del objeto para enviar el mensaje
GsonUtil<Resource> gsonUtil = new GsonUtil();
...
```

4. Para cada uno de los mensajes que se envían se crea el `Resource` y lo transformamos en el contenido del mensaje que se enviará al *broker*.

```java
...
for (int i = 0; i < NUM_MSG; ++i) {
    Resource resource = new Resource(iD+"-"+i,"uja_resource");
    TextMessage message = session.createTextMessage();
    message.setText(gsonUtil.encode(resource,Resource.class));
    producer.send(message);
    System.out.println(iD + " sent Job("+i+") " + resource);
}
...
```

5. Definimos la clase `SyncConsumer` de forma similar a la que se definió en el ejemplo del guión de la sesión 11.
6. El método `execution()` es el que tiene la adaptación necesaria para recibir los mensajes que incluyen en el contenido los `Resource` que deberá presentar por la consola.

```java
...
MessageConsumer consumer = session.createConsumer(destination);
// Creamos la utilizad para la recepción del objeto Java
GsonUtil<Resource> gsonUtil = new GsonUtil();
        
for (int i = 0; i < NUM_MSG; ++i) {
    TextMessage msg = (TextMessage) consumer.receive();
    Resource resource = gsonUtil.decode(msg.getText(), Resource.class);
    System.out.println(iD +" processing job: " + resource);
}
...
```

7. El método `main()` es el mismo al utilizado en el guión de la sesión 11.

## 12.2 Modos de envío para los mensajes JMS

En **JMS** tenemos dos formas para el envío y recepción de los mensajes:

-   Punto a Punto (**P2P**), en este modo el mensaje que se envía por un *producer* solo será recibido por un único *consumer*.
-   Publicación/Subscripción (**Pub/Sub**), en este modo el mensaje que se envía por un *producer* solo será recibido por múltiples *consumer*.

### 12.2.1 Modo P2P

En este modo **un mensaje se consume por un único consumidor** (1:1) pero no está limitado el número de emisores, *producer*, que envían mensajes a un *destino*. El *destino* del mensaje es una **cola** definida y con un nombre. Esto es, se trata de un modelo **FIFO**, el primer mensaje que se almacena en el destino es el primero que será enviado a un receptor, *consumer*. Al extraer el mensaje, el receptor envía un acuse de recibo a la cola para confirmar su correcta recepción (**ACK**).

Cada mensaje se envía a una cola específica, y los receptores reciben los mensajes de los destinos definidos. Estos destinos conservan todos los mensajes enviados hasta que son consumidos o hasta que expiren.

### 12.2.2 Modo Pub/Sub

En este modo **un mensaje puede consumirse por múltiples consumidores** (1:N). El destino de un mensaje se conoce como **tópico**. Un tópico no funciona como un pila, ya que los mensajes en los tópicos no se encolan. De hecho, un nuevo mensaje en el tópico sobrescribirá cualquier mensaje existente. Así pues, bajo este modelo de difusión, los emisores, *producer*, publican el mensaje en un tópico, y los receptores, *consumer*, se subscriben al tópico.

En este modelo, los publicadores (emisores) y los subscriptores (receptores) normalmente son anónimos y pueden, de forma dinámica, publicar o subscribirse a la jerarquía de contenidos. El sistema de mensajería se encarga de distribuir los mensajes que llegan al tópico de los múltiples publicadores a sus respectivos subscriptores, mediante un mecanismo **push**, de modo que los mensajes se envían automáticamente a los subscriptores.

La menearía **Pub/Sub** tiene las siguientes características:

-   Cada mensaje puede tener múltiples consumidores
-   Existe un dependencia temporal entre los publicadores y los subscritores perecederos (*non-durable*) ya que un cliente que se subscribe a un tópico puede consumir los mensajes publicados después de la subscripción, y el subscriptor debe continuar conectado para consumir los posteriores mensajes.

### 12.2.3 Ejemplo de utilización

En el ejemplo tenemos procesos `Publisher` envían un número de mensajes con un contenido de `Resource` a un tópico donde previamente se han suscrito los procesos `Subscriber` para una recepción síncrona.

1. Definimos la clase `Publisher` como se ha definido previamente la clase `Producer` del ejemplo anterior.
2. Como el modo de comunicación es del tipo **Pub**/**Sub** en el método `before()` hay que crear la conexión a un tópico destino.

```java
...
destination = session.createTopic(TOPIC);
...
```

3. El resto del proceso no tiene ningún cambio respecto al la clase `Producer` del ejemplo anterior.
4. Definimos la clase `SyncSubscriber` de forma similar a la clase `SyncConsumer` del ejemplo anterior.
5. Al igual que en la clase `Publisher` tenemos que modificar el método `before()` para crear la conexión al tópico destino. La conexión es igual que la definida en la clase `Publisher`.
6. Se modifica el método `execution()` para que se reciban todos los mensajes que se enviarán al tópico por las tareas `Publisher`. En el ejemplo los suscripciones se han realizado antes de que se hayan enviado mensajes al destino. Es decir, están esperando a que se envíen mensajes. Pero en este modelo de comunicación los suscriptores recibirán los mensajes que se envíen al destino una vez que el suscriptor se conecte al destino.

```java
...
for (int i = 0; i < (NUM_MSG * PUBLISHER); ++i) {
    TextMessage msg = (TextMessage) consumer.receive();
    Resource resource = gsonUtil.decode(msg.getText(), Resource.class);
    System.out.println(iD + " processing job: " + resource);
}
...
```

7. El método `main()` es el mismo que en el ejemplo anterior adaptado a las clases que se han definido para este ejemplo.

## 12.3 Recepción asíncrona de mensajes

Queremos que un receptor para un destino establecido establezca un *oyente* (listener) que se encargue de la recepción de los mensajes cuando estén disponibles. De esta forma el emisor puede ocuparse de otras tareas independientemente de la recepción de los mensajes. Con este modo para la recepción de los mensajes el programador debe sincronizar apropiadamente la ejecución de la tarea para poder utilizar el contenido de los mensajes que recibe. Es decir, la ejecución del receptor no se detendrá a la espera de los mensajes como en el caso de la recepción síncrona.

### 12.3.1 Definir un *oyente* (listener)

Tenemos que definir una clase que implemente la interface [`MessageListener`](https://docs.oracle.com/javaee/7/api/javax/jms/MessageListener.html "interface in javax.jms") y esto impone que se implemente el método `onMessage(Message message)` que se invoca cada vez que se recibe un mensaje en el destino.

1. Definimos una clase `MensajeListener<T>` que implementa la interface `MessageListener`.
2. Definimos las variables de instancia:
	- El nombre de la tarea receptora de los mensajes.
	- El tipo de la clase que se envía en el contenido del mensaje.
	- El objeto **Gson** para la transformación del contenido del mensaje.
3. Implementamos el método `onMessage(Message message)` donde se comprueba que el mensaje es un mensaje de texto para poder transformar el contenido a un objeto Java y presentar ese objeto a la consola.

```java
public void onMessage(Message message) {
    try {
        if (message instanceof TextMessage) {
            TextMessage textMessage = (TextMessage) message;
            T obj = gsonUtil.decode(textMessage.getText(), typeParameterClass);
            System.out.println(consumerName + " processing job: " + obj);
        } else
            System.out.println(consumerName + " Unknown message");
        } catch (JMSException e) {
            e.printStackTrace();
    }
}
```

### 12.3.3 Ejemplo de utilización

Este ejemplo es una modificación para el ejemplo anterior del guión. El receptor ya no recibe un número de mensajes establecidos de la suscripción. Estará activo por un tiempo y en ese tiempo recibirá todos los mensajes que se envíen. Es una sincronización muy básica. En el siguiente ejemplo se mostrarán otros modos de sincronización.

1. La clase `Publisher` es la misma que la del [ejemplo anterior](https://gitlab.com/ssccdd/guionsesion12#1223-ejemplo-de-utilizaci%C3%B3n).
2. Definimos la clase `AsyncSubscriber` como en el ejemplo anterior modificando el método `execution()`.
3. Hay que establecer el *oyente* para los mensajes de la suscripción al tópico que está suscrito el receptor.

```java
...
MessageConsumer consumer = session.createConsumer(destination);
        
consumer.setMessageListener(new MensajeListener<Resource>(iD, Resource.class));
...
```

4. El receptor espera por un tiempo. En ese tiempo se presentan por la consola todos los `Resource` que se han enviado al tópico desde que el receptor inició la suscripción.

```java
...
// Espera antes de finalizar
TimeUnit.MINUTES.sleep(TIME_SLEEP);
        
connection.stop();
consumer.close();
...
```

5. El resto de la aplicación no tiene ningún cambio respecto al [ejemplo anterior](https://gitlab.com/ssccdd/guionsesion12#1223-ejemplo-de-utilizaci%C3%B3n).

## 12.4 Implementación para la sentencia `select`

En Java no tenemos una equivalencia de código directa para la sentencia `select` que utilizamos para las soluciones teóricas de los ejercicios. Voy a presentar una implementación posible para simular de una forma adecuada el comportamiento esperado de la sentencia `select` en Java para el intercambio de mensajes **JMS**.

### 12.4.1 Ejemplo teórico 

Hay que organizar el paso por un puente con orientación norte/sur. Por el puente solo pueden circular coches en un sentido. Los coches en el mismo sentido pueden cruzar el puente a la vez, pero los coches en el sentido opuesto no. Para la solución suponemos que los coches están modelados como procesos.

- Hay que organizar el tráfico para que se permita el acceso en una dirección para un `MAX_COCHES` si hay coches esperando en la dirección contraria.

#### Tipos de datos para la solución

```
Direccion {Norte, Sur, Libre}
Tipo {Norte, Sur}
MsgCoche // Contenido del mensaje a enviar
    String : nombre
    Tipo : tipoCoche

// Constantes
PRIMERO = 1
NINGUNO = 0
MAX_COCHES = 5
```

#### Buzones

```
accesoPuente[Tipo] 
salidaPuente[Tipo]
respuesta[nombre] // Para confirmar el acceso
```

#### Proceso Coche(nombre,Tipo)

```
run {
    send(accesoPuente[Tipo], msg(nombre,Tipo))
    recive(respuesta[nombre], msg)

    pasarPuente()

    send(salidaPuente[Tipo], msg(nombre,Tipo))
}
```

#### Proceso Controlador

```
Variables
    int[Tipo] cochesEnPuente            // se inicializa a NINGUNO 
                                        // cuenta los coches en el puente
    Direccion ocupado = Libre           // ocupación del puente 
    int[Tipo] cochesSeguidos = NINGUNO  // controla el máximo permitido

ejecucion {
    while(true) {
        select{
            when ocupado != Sur && cochesSeguidos[Norte] < MAX_COCHES
            recive(accesoPuente[Norte], msg)
            cochesEnPuente[Norte]++
            cochesSeguidos[Norte]++
            if( cochesEnPuente[Norte] == PRIMERO )
                ocupado = Norte
            send(respuesta[msg.nombre], msg)
        OR
            recive(salidaPuente[Norte], msg)
            cochesEnPuente[Norte]--
            if( cochesEnPuente[Norte] == NINGUNO ) {
                ocupado = Libre
                if( accesoPuente[Sur].empty() ) {
                    // No hay pendientes en sentido contrario
                    cochesSeguidos[Norte] = NINGUNO
                    cochesSeguidos[Sur] = NINGUNO
                } else
                    // Damos paso al sentido contrario
                    cochesSeguidos[Sur] = NINGUNO
            }
        OR
            when ocupado != Norte && cochesSeguidos[Sur] < MAX_COCHES
            recive(accesoPuente[Sur], msg)
            cochesEnPuente[Sur]++
            cochesSeguidos[Sur]++
            if( cochesEnPuente[Sur] == PRIMERO )
                ocupado = Norte
            send(respuesta[msg.nombre], msg)
        OR
            recive(salidaPuente[Sur], msg)
            cochesEnPuente[Sur]--
            if( cochesEnPuente[Sur] == NINGUNO ) {
                ocupado = Libre
                if( accesoPuente[Norte].empty() ) {
                    // No hay pendientes en sentido contrario
                    cochesSeguidos[Norte] = NINGUNO
                    cochesSeguidos[Sur] = NINGUNO
                } else
                    // Damos paso al sentido contrario
                    cochesSeguidos[Norte] = NINGUNO
            }
        }
    }
}
```

### 12.4.2 Implementación ejemplo

Para la implementación del ejemplo utilizaremos una conexión **P2P** porque los mensajes solo tienen que ser tratados una única vez. Además, como la tarea que se encarga del control de acceso del puente tiene que esperar hasta que tenga solicitudes que atender se elige una recepción síncrona de los mensajes por ser más sencilla su utilización y cumple con los requerimientos del ejemplo. Esto no quiere decir que no se pueda utilizar una recepción asíncrona de los mensajes.

#### Constantes del ejemplo

En la interface `Constantes` se incluyen las constantes necesarias para la solución del ejemplo como se han analizado en la solución teórica.

```java
...
// Constantes cuarto ejemplo
public enum Direccion {NORTE, SUR, LIBRE}
public enum Tipo {NORTE, SUR}
public static final Tipo[] TIPOS = Tipo.values();
public static final int MAX_COCHES = 3;
public static final int COCHES = 20;
public static final int TIEMPO_LLEGADA = 3;
public static final int TIEMPO_PUENTE = 4;
public enum Servicio {
    ENTRADA(".entradaPuente."),
    SALIDA(".salidaPuente."),
    RESPUESTA(".respuesta.");
        
    private final String valor;

    private Servicio(String valor) {
        this.valor = valor;
    }

    public String getValor() {
        return valor;
    }
}
public static final Servicio[] SERVICIOS = Servicio.values();
...
```

Se define el enumerado `Servicio` para simular la implementación de las *cláusulas* de la sentencia `select` de la resolución teórica del ejercicio. Además se incluye un `String` que permite identificar los mensajes en una destino dentro del *broker*.

#### MsgCoche

Esta clase define el contenido que se incluirá en los mensajes que se intercambien las tareas. En el mensaje se incluye el nombre de la tarea que representa al coche y el sentido de circulación en el puente que tiene el coche.

#### Puente
 
Esta clase define las variables del `Proceso Controlador` que se ha definido en la solución teórica del ejercicio. Como las solicitudes de acceso al puente son concurrentes por los diferentes *servicios* de la `TareaControlador` se ha diseñado como un monitor. Con los siguientes procedimientos:

- `pendienteEntrada(Tipo tipoCoche)` : se controla el número de peticiones de acceso al puente en uno de los sentidos de circulación.

- `entradaPuente(Tipo tipoCoche)` : resuelve las solicitudes de entrada al puente para los coches en uno de los sentidos de circulación.

- `salidaPuente(Tipo tipoCoche)` : registra la salida del puente para los coches en uno de los sentidos de circulación.

#### TareaCoche

Esta tarea implementa el comportamiento del `Proceso Coche(nombre, Tipo)`. Las acciones que tiene que realizar son:

1. Implementamos el método `before()` que configura la conexión con el *broker*. Se establece el modo de conexión como **P2P** y los destinos:
	- Para la entrada y salida del puente se configuran con las características del servicio y la dirección para la circulación.
	- Para la respuesta del servidor se configura con las características del servicio y el nombre de la tarea. De esta forma recibirá una confirmación individual para garantizar el acceso al puente.

2. Implementamos el método `execution()`  que es el cuerpo de ejecución de la tarea 
3. Se crea el contenido del mensaje que se enviará:

```java
// Creamos la instancia del objeto para enviar el mensaje
GsonUtil<MsgCoche> gsonUtil = new GsonUtil();
contenido = new MsgCoche(name,tipoCoche);
```

4. Se envía el mensaje para la solicitud de acceso al puente:

```java
// Simulamos tiempo para llegar al puente
TimeUnit.SECONDS.sleep(rnd.nextInt(TIEMPO_LLEGADA)+1);
        
// Acceso al Puente
producer = session.createProducer(destination[ENTRADA.ordinal()]);
msg = session.createTextMessage(gsonUtil.encode(contenido, MsgCoche.class));
producer.send(msg);
producer.close();
System.out.println(name + " Espera acceso al puente " + contenido);
```

5. Espera la confirmación del `Controlador` para simular el paso por el puente

```java
// Espera respuesta para el acceso
consumer = session.createConsumer(destination[RESPUESTA.ordinal()]);
msg = (TextMessage) consumer.receive();
contenido = gsonUtil.decode(msg.getText(), MsgCoche.class);
System.out.println(name + " Circula por el puente " + contenido);
        
// Simulamos el tiempo de paso del puente
TimeUnit.SECONDS.sleep(rnd.nextInt(TIEMPO_PUENTE)+1);
```

6. Envía el mensaje para comunicar que se ha cruzado el puente:

```java
// Abandona el puente
producer = session.createProducer(destination[SALIDA.ordinal()]);
msg = session.createTextMessage(gsonUtil.encode(contenido, MsgCoche.class));
producer.send(msg);
producer.close();
System.out.println(name + " Abandona el puente " + contenido);
```

7. Implementamos el método `after()` para finalizar la conexión con el *broker*.
8. Implementamos el método `run()` donde se ejecutan los métodos anteriores como la ejecución que sigue la `TareaCoche`. Se deben tratar las excepciones que se pueden producir durante la ejecución de la tarea.

#### TareaControlador

Esta tarea implementa el `Proceso Controlador` de la solución teórica e implementa la interface `Runnable`. Como variables de instancia se definen:

- Nombre de la tarea.
- Referencia al `Puente` del cual controla su acceso.
- Un objeto de sincronización para que la aplicación pueda indicar que ha finalizado su ejecución.
- Un **marco de ejecución** donde se ejecutarán las tareas que simulan las *cláusulas* de la sentencia `select`.
- Una lista donde se almacenan las tareas que implementan las *cláusulas* de la sentencia `select` para que se puedan finalizar cuando el controlador finalice su ejecución.

1. Implementamos el método `run()` donde se simula el comportamiento que tendrá la sentencia `select` de la solución teórica.

```java
public void run() {
    System.out.println(name + " Comienza su ejecución...");
        
    for(Servicio servicio : SERVICIOS)
        switch ( servicio ) {
            case ENTRADA:
                for(Tipo tipoCoche : TIPOS)
                    entrada(servicio,tipoCoche);
                break;
            case SALIDA:
                for(Tipo tipoCoche : TIPOS)
                    salida(servicio,tipoCoche);
                break;
        }
            
    finControlador();
        
    System.out.println(name + " Finaliza su ejecución...");
}
```

2. Implementamos el método `entrada()` que ejecutará las tareas que implementan las *cláusulas* de la sentencia `select` para las peticiones de entrada al puente en los diferentes sentidos de circulación.
3. Implementamos el método `salida()` que ejecutará las tareas que implementan las *cláusulas* de la sentencia `select` para comunicar la salida del puente en los diferentes sentidos de circulación.
4. Implementamos el método `finControlador()` para finalizar de forma ordenada la tarea cuando el hilo principal solicite la finalización de la `TareaControlador`. Para ello se utiliza un elemento de sincronización con el hilo principal.

```java
private void finControlador() {
    try {
        finControlador.await();
    } catch (InterruptedException ex) {
        // No hacer nada porque se está terminando
    }
        
    // Se solicita la finalización de las tareas de control
    for(Future<?> tarea : listaTareas)
        tarea.cancel(true);
        
    ejecucion.shutdown();
    try {
        ejecucion.awaitTermination(TIME_SLEEP, TimeUnit.DAYS);
    } catch (InterruptedException ex) {
        // No hacer nada porque se está terminando
    }
}
```

#### EntradaPuente

Es una tarea que implementa la *cláusula* de la sentencia `select` para gestionar el acceso al puente por los coche en un sentido de la circulación. Implementa la interface `Runnable` para garantizar que se pueden atender las solicitudes de acceso según se producen y no en un orden establecido, como el funcionamiento dentro de la sentencia `select`.  Las variables de instancia necesarias son:

- El nombre del controlador.
- El `String` necesario para definir el destino de los mensajes en el *broker*.
- El `Puente` para el que se controla su acceso.

1. Implementamos el método `run()` donde se incluyen los métodos necesarios el intercambio de mensajes con el *broker* en los destinos conocidos mediante una conexión **P2P** y recepción síncrona. Se da una respuesta a las posibles excepciones que se pueden producir durante la ejecución de la tarea.
2. La ejecución de la tarea está en un ciclo infinito hasta que se solicite la interrupción como método de finalización. De esta forma se atienden a todas las peticiones de entrada al puente mientras dure la ejecución de la tarea.
3. La implementación del método `execution()` gestiona las peticiones de acceso al puente para el destino establecido en su ejecución.
4. Espera a recibir un mensaje para el destino en el *broker* de forma síncrona.

```java
 MessageConsumer consumer = session.createConsumer(destination);
 // Creamos la utilizad para la recepción del objeto Java
 GsonUtil<MsgCoche> gsonUtil = new GsonUtil();
        
 // Solicitud de entrada al puente
 msg = (TextMessage) consumer.receive();
 MsgCoche coche = gsonUtil.decode(msg.getText(), MsgCoche.class);
 consumer.close();
```

5. Se registra la solicitud de acceso al puente.
6. Se actualizan los datos para la gestión de acceso al puente como método de sincronización. Cuando se complete la actualización de información se envía la confirmación para que el coche pueda acceder al puente en el sentido solicitado. Para la confirmación se utiliza el nombre de la `TareaCoche` que realizó la solicitud y así sincronizar el acceso.

```java
// Indica la intención para cruzar el puente
puente.pendienteEntrada(coche.getTipoCoche());
        
// Revuelve el acceso al puente
puente.entradaPuente(coche.getTipoCoche());
        
// Se confirma el acceso al puente
Destination respuesta = session.createQueue(QUEUE + RESPUESTA.getValor() + coche.getName());
MessageProducer producer = session.createProducer(respuesta);
msg = session.createTextMessage(gsonUtil.encode(coche, MsgCoche.class));
producer.send(msg);
producer.close();
```

#### SalidaPuente

Es una tarea que implementa la *cláusula* de la sentencia `select` para que los coches comuniquen que han cruzado el puente en un sentido de la circulación. El diseño de la tarea es similar a `EntradaPuente`, es decir, tiene las mismas variables de instancia. Y los métodos tienen el mismo código salvo:

1. El método `execution()` que espera recibir un mensaje en el destino que se estableció en la configuración de la conexión con el *broker*. Espera un mensaje que indicará que un coche ha abandonado el puente en un sentido de circulación.

```java
MessageConsumer consumer = session.createConsumer(destination);
// Creamos la utilizad para la recepción del objeto Java
GsonUtil<MsgCoche> gsonUtil = new GsonUtil();
        
// Informa de la salida del puente
msg = (TextMessage) consumer.receive();
MsgCoche coche = gsonUtil.decode(msg.getText(), MsgCoche.class);
consumer.close();
```

2. Para finalizar el método se actualiza el estado del puente.

```java
// Se actualiza el estado del puente
puente.salidaPuente(coche.getTipoCoche());
```

---
[^nota1]: Para una documentación más extensa consultar *Instant GSON*, *ActiveMQ in Action* y *Java Message Service, 2nd Edition* que se encuentra disponible para los alumnos de la [Universidad de Jaén](https://www.ujaen.es/) por medio de su servicio de [Biblioteca Digital](http://www.ujaen.debiblio.com/login?url=https://learning.oreilly.com/home/).
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTIwMjkwMTk0NTksLTI5NDE4MzcxNSw2NT
Q4MjQ0MzUsNTgwNzM3NjU3LC0xMTcwMjU4OTI0LDk2MDgyNTU5
OCwxNjgzNzgwNTUwLDE4MTMyODYzODQsLTI2ODQ2ODk3Ml19
-->