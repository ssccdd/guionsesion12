package es.uja.ssccdd.guionsesion12.ejemplo3;

import es.uja.ssccdd.guionsesion12.GsonUtil;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

/**
 * Ejemplo de procesamiento basico de una interfaz listener para recibir mensajes en JMS
 * @author javier medina
 *
 */
public class MensajeListener<T> implements MessageListener {
    private final String consumerName;
    private final Class<T> typeParameterClass;
    private final GsonUtil<T> gsonUtil;

    public MensajeListener(String consumerName, Class<T> typeParameterClass) {
        this.consumerName = consumerName;
        this.typeParameterClass = typeParameterClass;
        this.gsonUtil = new GsonUtil();
    }
	
    
    /**
     * Interfaz para obtener el texto
    */	
    public void onMessage(Message message) {
	try {			
            if (message instanceof TextMessage) {		
                TextMessage textMessage = (TextMessage) message;
                T obj = gsonUtil.decode(textMessage.getText(), typeParameterClass);
		System.out.println(consumerName + " processing job: " + obj);
            } else
                System.out.println(consumerName + " Unknown message");
	} catch (JMSException e) {			
            e.printStackTrace();
	}	
    }
}